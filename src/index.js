import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.min.css";
import "antd/dist/antd.variable.min.css";
// import "antd/dist/antd.variable.min.css";
// import "./Scss/base/_colors.scss";
import "./index.scss";
import App from "./App";
import "./i18n";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "../src/store/store";
import ConfigProviderComponent from "./ConfigProvider";
import ErrorBoundary from "./ErrorBoundary/ErrorBoundary";

window.addEventListener("storage", () => {
  // window.location.reload(true);
  console.log(localStorage.getItem("persist:root"));
  if (localStorage.getItem("persist:root") === null) {
    console.log("inside if");
    window.location.reload(true);
  }
})

ReactDOM.render(
  <ConfigProviderComponent>
    {/* <ErrorBoundary> */}
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
    {/* </ErrorBoundary> */}
    ,
  </ConfigProviderComponent>,
  document.getElementById("root"),
);
